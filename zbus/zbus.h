#ifndef __ZBUS_H_
#define __ZBUS_H_

#include "platform.h" 

#ifdef __cplusplus
extern "C" {
#endif

typedef struct zbus zbus_t;
 

#define PUB  "PUB" 
#define SUB  "SUB" 

#define REG	"REG"
#define HBT "HBT"
#define DIE "DIE" 
#define MSG "MSG" 



#ifdef __cplusplus
}
#endif

#endif
